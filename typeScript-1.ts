//1
let compare = function (a: string, b: string): number {
    return 8;
};

function anagram (test1: string, test2: string) {
    let arrAnagram1: Array<string> = test1.split('');
    let arrAnagram2: Array<String> = test2.split('');
    arrAnagram1.sort(compare);
    arrAnagram2.sort();
    return compare;
}
anagram('сон', 'нос');
//2
function isAnagmamma(word1: string, word2: string) {
    if (word1.length !== word2.length) {
        return false;
    }

    for (let i: number = 0; i < word1.length; i++) {
        let result1 = word1[i];
        let counter1: number = 0;
        let counter2: number = 0;

        for (let j: number = 0; j < word1.length; j++) {
            let result2 = word1[j];
            if (result1 === result2) {
                counter1++;
            }

            result2 = word2[j];
            if (result1 === result2) {
                counter2++;
            }
        }
        if (counter1 !== counter2) {
            return false;
        }
    }
    return true;
}
isAnagmamma('сон', 'нос');
//3
interface Obj {
    [key: string]: number;
}
const getDataNumber = (numb: number)=> {
    let arrNumbers: Array<string> = ('' + numb).split('');
    let countNumb: Array<number> = [];
    let obj: Obj = {};

    for (let i: number = 0; i < arrNumbers.length; i++) {
        countNumb[i] = 0;

        for (let j: number = 0; j < arrNumbers.length; j++) {
            if (arrNumbers[i] === arrNumbers[j]) {
                countNumb[i] = countNumb[i] + 1;
                obj[arrNumbers[i]] = countNumb[i];
            }
        }
    }

    return obj;
};
getDataNumber(45548487);
//4
interface I {
    [key: string]: number;
}
const countExclusiveWords = (param: string) => {
    let arrWords: Array<string> = param.split(' ');
    let countWords: Array<number>  = [];
    let obj: I = {};

    for (let i: number = 0; i < arrWords.length; i++) {
        countWords[i] = 0;

        for (let j: number = 0; j < arrWords.length; j++) {
            if (arrWords[i] === arrWords[j]) {
                countWords[i] = countWords[i] + 1;
                obj[arrWords[i]] = countWords[i];
            }
        }
    }

    return obj;
};
countExclusiveWords('слово текст текст тест');
//5
interface A {
    [key: string]: number;
}
const getWordsStr = (str: string) => {
    let newStr: Array<string> = str.split(' ');
    let obj: A = {};

    for (let i: number = 0; i < newStr.length; i++) {
        if (obj[newStr[i]] === undefined) {
            obj[newStr[i]] = 1;
        } else {
            obj[newStr[i]]++;
        }
    }
    return obj;
};
getWordsStr('hello hello this is test test test');
//6
const fib = (num: number) => {
    let prev: number = 0, next: number = 1;

    for(let i: number = 0; i < num; i++){
        let temp = next;
        next = prev + next;
        prev = temp;
    }
    return prev;
};
fib(8);
//7
class Rectangle {
    length: number;
    width: number;
    constructor(length: number, width: number) {
        this.length = length;
        this.width = width;
    }

    findPerimeter(){
        return 2*(this.length + this.width);
    }

    findSquare(){
        return this.length * this.width;
    }
}
let perimeter = new Rectangle(10,30);
perimeter.findPerimeter();
perimeter.findSquare();
//8
class Triangle {
    side1: number;
    side2: number;
    side3: number;
    constructor(side1: number, side2: number, side3: number) {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }

    findPerimeter(){
        return  this.side1 + this.side2 + this.side3;
    }

    findSquare(){
        return 1 / 2 * this.side1 * this.side2 * Math.sin((this.side3 * Math.PI) / 180);
    }
}

let triangle = new Triangle(10,30, 30);
triangle.findPerimeter();
triangle.findSquare();
//9
class Circle {
    radius: number;
    constructor(radius: number) {
        this.radius = radius;
    }

    findPerimeter(){
        return Math.PI * this.radius * this.radius;
    }

    findSquare(){
        return 2 * Math.PI * this.radius;
    }
}

let circle = new Circle(30);
circle.findPerimeter();
circle.findSquare();
//10
const factorialNumb = (num: number) => {
    let result: number = 1;

    for (let i: number = 2; i <= num; i++)
        result = result * i;
    return result;
};
factorialNumb(5);
//11
function summaNumber(arr: Array<number>, callback: (value: number) => boolean) {
    let result: number = 0;

    for (let i: number = 0; i < arr.length; i++) {
        if (callback(arr[i])) {
            result += arr[i];
        }
    }

    return result;
}
summaNumber([1,2,0,4,0,6,7,8,67,4,23], (value) => value % 2 === 0);
//12
function countsElemArr(arrayNumb: Array<number>, callback: (value: number) => boolean) {
    let result: number = 0;

    for (let i: number = 0; i < arrayNumb.length; i++) {
        if (callback(arrayNumb[i])) {
            result++;
        }
    }

    return result;
}
countsElemArr([-5, 0, 8, 2, -10, -3, 0, 0, 0, 6, 7, 8], (value) => value % value === 0);
//13
const transformBinary = (numb: number) => {
    let divider: number = 2;
    let binaryNumber: string = '';

    for (let i:number = 0; i < numb;) {
        binaryNumber += numb % divider;
        numb = Math.floor(numb / divider);
    }

    return binaryNumber.split('').reverse().join('');
};
transformBinary(45);
//14
const transformDecimal = (numb: number) => {
    let binaryNumber: Array<string> | any = ('' + numb).split('').reverse();
    let binaryData: Array<number> = [];
    for (let i: number = 0; i < binaryNumber.length; i++) {
        binaryData[i] =  binaryNumber[i] * Math.pow(2, i)
    }
    let numbDecimal: number = 0;

    for (let j: number = 0; j < binaryData.length; j++) {
        numbDecimal += binaryData[j];

    }
    return numbDecimal;
};
transformDecimal(101101);
//15
function summaArrayNum(arr: Array<number>[], callback: (value: number) => boolean) {
    let result: number = 0;

    for (let i: number = 0; i < arr.length; i++) {
        for (let j: number = 0; j < arr[i].length; j++) {
            if (callback(arr[i][j])) {
                result += arr[i][j];
            }
        }
    }

    return result;
}
summaArrayNum([[5, 8, 8],[2, 5, 3]], (value) => value % 2 === 0);
//16
function countsElem(arrayNumb: Array<number>[], callback: (value: number) => boolean) {
    let result: number = 0;

    for (let i: number = 0; i < arrayNumb.length; i++) {
        for (let j: number = 0; j < arrayNumb[i].length; j++) {
            if (callback(arrayNumb[i][j])) {
                result++;
            }
        }
    }

    return result;
}
countsElem([[5, 8, -8],[2, -5, 3]], (value) => value < 0);
//17
function sum(min: number, max: number, callback: (value: number) => boolean, sum?: number) {
    sum = sum || 0;

    for (let i: number = min; i <= max; i++) {
        if (callback(i)) {
            sum += min;
        }
    }
    return sum;
}
sum(-20, 50,(value) => value % 3 === value);
//18
function averageNumber(numbArr: Array<number>, callback: (value: number) => boolean) {
    let average: number = 0;
    let count: number = 0;

    for (let i: number = 0; i < numbArr.length; i++) {
        if(callback(numbArr[i])) {
            average += numbArr[i];
            count += 1;
        }
    }
    return average  / count;
}
averageNumber([2, 2, 5, 8, 5, 5], (value) => value % 2 !== 0);
//19
function averageNums(numbArr: Array<number>[], callback: (value: number) => boolean) {
    let average: number = 0;
    let count: number = 0;

    for (let i: number = 0; i < numbArr.length; i++) {
        for (let j: number = 0; j < numbArr[i].length; j++) {
            if (callback(numbArr[i][j])) {
                average += numbArr[i][j];
                count += 1;
            }

        }
    }

    return average / count;
}
averageNums([[5, 2, 8], [7, 6, 8]], (value) => value % 2 !== 0);
//20
const transMatrix = (arr: Array<number>[]) => {
    let newArr: Array<number>[] = [];

    for (let i: number = 0; i < arr[0].length; i++) {
        newArr[i] = [];
        for (let j: number = 0; j < arr.length; j++)
            newArr[i][j] = arr[j][i];
    }
    return newArr;
};
transMatrix([
    [1,2,3],
    [1,2,3],
    [1,2,3],
]);
//21
const sumMatrices = (arr1: Array<number>[], arr2:Array<number>[]) => {
    let result: Array<number>[] = [];

    for (let i: number = 0; i < arr1.length; i++) {
        result[i] = [];

        for (let j: number = 0; j < arr2[0].length; j++) {
            let sum: number = 0;

            for (let k: number = 0; k < arr1[0].length; k++) {
                sum += arr1[i][k] + arr2[k][j];
            }
            result[i][j] = sum;
        }
    }
    return result;
};

let arr1 = [[1,2],[3,4]];
let arr2 = [[5,6],[7,8]];

sumMatrices(arr1, arr2);
//22
const removeString = (arr: Array<number>[]) => {
    let newArr: Array<number> = [];

    for (let i: number = 0; i < arr.length; i++) {
        let isZero: boolean = true;

        for (let j: number = 0; j < arr[i].length; j++) {
            if (arr[i][j] === 0) {
                break;
            }
        }
        if (isZero) {
            newArr = (arr[i]);
        }

    }
    return newArr;
};
removeString([[1,0,3], [1,2,3]]);
//23
const removeStr = (arr: Array<number>[]) => {
    let newArr: Array<number> = [];

    for (let i: number = 0; i < arr.length; i++) {
        let isZero: boolean = true;

        for (let j: number = 0; j < arr[i].length; j++) {
            if (arr[i][j] === 0) {
                break;
            }
        }
        if (isZero) {
            newArr = (arr[i]);
        }

    }
    return newArr;
};
removeStr([[1,0,3], [1,2,3]]);
//24
interface Object {
    [key: string]: number;
}
function getNumber(numb: number, index?: number, obj?: Object): {} {
    obj = obj || {};
    index = index || 0;
    let arrNumbers: Array<string> = ('' + numb).split('');
    let searchNumb: Array<number> | string = arrNumbers[index];

    if (index > arrNumbers.length -1) {
        return obj;
    }

    let count: number = 0;

    for (let i: number = 0; i < arrNumbers.length; i++) {
        if (arrNumbers[i] === searchNumb) {
            count++;
            obj[arrNumbers[i]] = count;
        }
    }

    return getNumber(numb, ++index, obj);
};
getNumber(45548487);
//25
function transformBinaryRec(numb: number, binaryNumber: string = ''): string {
    let divider: number = 2;

    if (0 < numb) {
        binaryNumber += numb % divider;
        numb = Math.floor(numb / divider);
        return transformBinaryRec(numb, binaryNumber);
    }
    return binaryNumber.split('').reverse().join('');
};
transformBinaryRec(45);
//26
function transformDecimalNumb(numb: number, index?: number, binaryNumber: Array<string> | any = [], binaryData: Array<number> = []): {} {
    index = index || 0;
        binaryNumber = ('' + numb).split('').reverse();

    if (index < binaryNumber.length) {
        binaryData[index] =  binaryNumber[index] * Math.pow(2, index);
        return transformDecimalNumb(numb, binaryNumber, ++index, binaryData);
    }
    let numbDecimal: number = 0;

    for (let j: number = 0; j < binaryData.length; j++) {
        numbDecimal += binaryData[j];

    }
    return numbDecimal;
};
transformDecimalNumb(101101);
//27
function getFibonachi(n: number): number {
    if (n === 0) {
        return 0;
    }
    if (n === 1) {
        return 1;
    }

    return getFibonachi(n - 1) + getFibonachi(n - 2);

}
getFibonachi(8);
//28
function getFactorial(n: number): number {
    if (n === 1) {
        return 1;
    }

    return n * getFactorial(n - 1);

}
getFactorial(4);
//29
interface Count {
    [key: string]: number;
}
const countWord: Function = (word: string, index: number = 0, obj: Count = {}) => {
    let arrWords: Array<string> = word.split(' ');
    let countWords: Array<string> | any = arrWords[index];

    if (index > arrWords.length -1) {
        return obj;
    }

    let count: number = 0;

    for (let i: number = 0; i < arrWords.length; i++) {
        if (arrWords[i] === countWords) {
            count++;
            obj[arrWords[i]] = count;
        }
    }

    return countWord(word, ++index, obj);
};
countWord('слово текст текст тест');
//30
interface C {
    [key: string]: number;
}
const countWordStr: Function = (str:string, index: number = 0, objWord: C = {}) => {
    let arrWords: Array<string> = str.split(' ');
    let countWords: Array<number> | any = arrWords[index];

    if (index > arrWords.length -1) {
        return objWord;
    }

    let count: number = 0;

    for (let i: number = 0; i < arrWords.length; i++) {
        if (arrWords[i] === countWords) {
            count++;
            objWord[arrWords[i]] = count;
        }
    }

    return countWordStr(str, ++index, objWord);
};
countWordStr('hello hello this is test test test');
//31
const summ: Function = (arr: Array<number>, callback: (value: number) => boolean, sum?: number, index?: number) => {
    index = index || 0;
    sum = sum || 0;

    if (index < arr.length) {
        if (callback(arr[index])) {
            sum += arr[index];
            return summ(arr,callback, sum, ++index);
        }else {
            return summ(arr, callback, sum, ++index);
        }
    }
    return sum;

};
summ([1,2,3,4,5,6,7,8],(value: number) => value % 3 === 0);
//32
const countNumb: Function = (arr: Array<number>,callback: (value: number) => boolean, count?: number, index?: number) => {
    index = index || 0;
    count = count || 0;

    if (index < arr.length) {
        if (callback(arr[index])) {
            count++;
            return countNumb(arr,callback, count, ++index);
        }else {
            return countNumb(arr,callback, count, ++index);
        }
    }
    return count;

};
countNumb([1,2,-3,4,-5,-6,7,8], (value: number) => value > 0);
//33
const sumMinMaxValue: Function = (min: number, max: number, callback: (value: number) => boolean, sum?: number) => {
    sum = sum || 0;
    sum = sum + min;

    if (callback(min)) {
        return sum;
    }

    return sumMinMaxValue(++min, max, callback, sum);
};
sumMinMaxValue(-3, 5, (value: number) => value === value);
//34
const sumValue: Function = (min: number, max: number, callback: (value: number) => boolean, sum?: number) => {
    sum = sum || 0;

    if (callback(min)) {
        sum = sum + min;
    }

    if (callback(min)) {
        return sum;
    }

    return sumValue(++min, max, callback, sum);
};
sumValue(-3, 5, (value: number) => value % 3 === value);
//35
const transformMatrix: Function = (arr: Array<number>[], newArr:  Array<number>[] = [], IndexRow?: number, IndexColumn?: number) => {
    IndexRow = IndexRow || 0;
    IndexColumn = IndexColumn || 0;

    if (IndexColumn === arr[0].length) {
        ++IndexRow;
        IndexColumn = 0;
    }
    if (IndexRow < arr[0].length) {
        if (IndexColumn < arr.length) {
            if(!newArr[IndexRow]){
                newArr[IndexRow] = [];
            }
            newArr[IndexRow][IndexColumn] = arr[IndexColumn][IndexRow];
            return transformMatrix(arr, newArr, IndexRow, ++IndexColumn);
        }
    }
    return newArr;
};
transformMatrix([
    [1,2,3],
    [1,2,3],
    [1,2,3],
]);
//memo
function memoize(fn: Function) {
    let cache: {} | any = {};

    return (...args: any) => {
        const cacheKey = JSON.stringify(args);
        if (!(cacheKey in cache)) {
            cache[cacheKey] = fn(...args);
        }
        return cache[cacheKey];
    };
}
//36
const transformToBinary: Function = (numb: number, binaryNumber: string = '') => {
    let divider: number = 2;

    if (0 < numb) {
        binaryNumber += numb % divider;
        numb = Math.floor(numb / divider);
        return transformToBinary(numb, binaryNumber);
    }
    return binaryNumber.split('').reverse().join('');
};
const memoTransformToBinary = memoize(transformToBinary);
memoTransformToBinary(45);
//37
const transformToDecimal: Function = (numb: number, binaryNumber: Array<string> | any = [], index?: number, binaryData: Array<number> = []) => {
    index = index || 0;
    binaryNumber = ('' + numb).split('').reverse();

    if (index < binaryNumber.length) {
        binaryData[index] =  binaryNumber[index] * Math.pow(2, index)
        return transformToDecimal(numb, binaryNumber, ++index, binaryData)
    }
    let numbDecimal: number = 0;

    for (let j: number = 0; j < binaryData.length; j++) {
        numbDecimal += binaryData[j];

    }
    return numbDecimal;
};
const memoTransformToDecimal = memoize(transformToDecimal);
memoTransformToDecimal(101101);
//38
const removeStrInArr: Function = (arr: Array<number>[], newArr: Array<number>[] = [], indexRow: number, indexColumn: number, haveNol: boolean = false) => {
    indexRow = indexRow || 0;
    indexColumn = indexColumn || 0;

    if (indexColumn === arr[0].length) {
        if(!haveNol){
            newArr.push(arr[indexRow]);
        }
        if (haveNol) {
            haveNol = false;
        }
        ++indexRow;
        indexColumn = 0;
    }
    if (indexRow < arr.length) {
        if (indexColumn < arr[indexRow].length) {
            if (arr[indexRow][indexColumn] === 0) {

                haveNol = true;
            }
            return removeStrInArr(arr, newArr, indexRow, ++indexColumn, haveNol);
        }
    }

    return newArr;
};
const memoRemoveStrInArr = memoize(removeStrInArr);
memoRemoveStrInArr(
    [
        [2,6,7],
        [1,0,3],
        [1,2,3],
        [0,10,8],
    ]);




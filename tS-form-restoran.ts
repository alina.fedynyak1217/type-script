export class Form {
    createForm() {
        let form = document.getElementById('form-employee') as HTMLFormElement;
        if (form.length !== undefined) {
            return;
        }

        let formCart = document.createElement('form') as HTMLFormElement;
        formCart.className = 'cart-form';
        formCart.id = 'cart-form';
        let formTitle = document.createElement('h1') as HTMLHeadingElement;
        formTitle.innerText = 'Form Add';
        let blockName = document.createElement('div') as HTMLDivElement;
        let nameLabel = document.createElement('label') as HTMLLabelElement;
        nameLabel.htmlFor = 'name';
        nameLabel.innerText = 'Name:';
        let nameInput = document.createElement('input') as HTMLInputElement;
        nameInput.type = 'text';
        nameInput.name = 'name';
        nameInput.id = 'name';
        nameInput.value = '';

        blockName.appendChild(nameLabel);
        blockName.appendChild(nameInput);

        formCart.appendChild(formTitle);
        formCart.appendChild(blockName);

        let blockSurname = document.createElement('div') as HTMLDivElement;
        let surnameLabel = document.createElement('label') as HTMLLabelElement;
        surnameLabel.htmlFor = 'surname';
        surnameLabel.innerText = 'Surname:';
        let surnameInput = document.createElement('input') as HTMLInputElement;
        surnameInput.type = 'text';
        surnameInput.name = 'surname';
        surnameInput.id = 'surname';
        surnameInput.value = '';

        blockSurname.appendChild(surnameLabel);
        blockSurname.appendChild(surnameInput);
        formCart.appendChild(blockSurname);

        let blockSalary = document.createElement('div') as HTMLDivElement;
        let salaryLabel = document.createElement('label') as HTMLLabelElement;
        salaryLabel.htmlFor = 'salary';
        salaryLabel.innerText = 'Salary:';
        let salaryInput = document.createElement('input') as HTMLInputElement;
        salaryInput.type = 'text';
        salaryInput.name = 'salary';
        salaryInput.id = 'salary';
        salaryInput.value = '';

        blockSalary.appendChild(salaryLabel);
        blockSalary.appendChild(salaryInput);
        formCart.appendChild(blockSalary);

        let blockDepartment = document.createElement('div') as HTMLDivElement;
        let departmentLabel = document.createElement('label') as HTMLLabelElement;
        departmentLabel.htmlFor = 'department';
        departmentLabel.innerText = 'Department:';
        let departmentSelect = document.createElement('select') as HTMLSelectElement;
        departmentSelect.id = 'department';
        departmentSelect.name = 'department';
        let departmentOptionBar = document.createElement('option') as HTMLOptionElement;
        departmentOptionBar.value = 'bar';
        departmentOptionBar.innerText = 'bar';
        let departmentOptionKitchen = document.createElement('option') as HTMLOptionElement;
        departmentOptionKitchen.value = 'kitchen';
        departmentOptionKitchen.innerText = 'kitchen';
        let departmentOptionHall = document.createElement('option') as HTMLOptionElement;
        departmentOptionHall.value = 'hall';
        departmentOptionHall.innerText = 'hall';

        blockDepartment.appendChild(departmentLabel);
        departmentSelect.appendChild(departmentOptionBar);
        departmentSelect.appendChild(departmentOptionKitchen);
        departmentSelect.appendChild(departmentOptionHall);
        blockDepartment.appendChild(departmentSelect);
        formCart.appendChild(blockDepartment);

        let blockPosition = document.createElement('div') as HTMLDivElement;
        let positionLabel = document.createElement('label') as HTMLLabelElement;
        positionLabel.htmlFor = 'position';
        positionLabel.innerText = 'Position:';
        let positionSelect = document.createElement('select') as HTMLSelectElement;
        positionSelect.id = 'position';
        positionSelect.name = 'position';
        let positionOptionBarmen = document.createElement('option') as HTMLOptionElement;
        positionOptionBarmen.value = 'barmen';
        positionOptionBarmen.innerText = 'barmen';
        let positionOptionCook = document.createElement('option') as HTMLOptionElement;
        positionOptionCook.value = 'cook';
        positionOptionCook.innerText = 'cook';
        let positionOptionWaite = document.createElement('option') as HTMLOptionElement;
        positionOptionWaite.value = 'waite';
        positionOptionWaite.innerText = 'waite';

        blockPosition.appendChild(positionLabel);
        positionSelect.appendChild(positionOptionBarmen);
        positionSelect.appendChild(positionOptionCook);
        positionSelect.appendChild(positionOptionWaite);
        blockPosition.appendChild(positionSelect);
        formCart.appendChild(blockPosition);

        let blockStatus = document.createElement('div') as HTMLDivElement;
        let statusLabel = document.createElement('label') as HTMLLabelElement;
        statusLabel.htmlFor = 'status';
        statusLabel.innerText = 'Status:';
        let statusSelect = document.createElement('select') as HTMLSelectElement;
        statusSelect.id = 'status';
        statusSelect.name = 'status';
        let statusOptionDismissed = document.createElement('option') as HTMLOptionElement;
        statusOptionDismissed.value = 'dismissed';
        statusOptionDismissed.innerText = 'dismissed';
        let statusOptionWorks = document.createElement('option') as HTMLOptionElement;
        statusOptionWorks.value = 'works';
        statusOptionWorks.innerText = 'works';

        blockStatus.appendChild(statusLabel);
        statusSelect.appendChild(statusOptionDismissed);
        statusSelect.appendChild(statusOptionWorks);
        blockStatus.appendChild(statusSelect);
        formCart.appendChild(blockStatus);

        let blockCheckbox = document.createElement('div') as HTMLDivElement;
        blockCheckbox.className = 'checkbox';
        let checkboxLabel = document.createElement('label') as HTMLLabelElement;
        checkboxLabel.htmlFor = 'supervisor';
        checkboxLabel.innerText = 'Supervisor:';
        let checkboxInput = document.createElement('input') as HTMLInputElement;
        checkboxInput.type = 'checkbox';
        checkboxInput.name = 'supervisor';
        checkboxInput.id = 'supervisor';

        blockCheckbox.appendChild(checkboxLabel);
        blockCheckbox.appendChild(checkboxInput);
        formCart.appendChild(blockCheckbox);

        let blockBtn = document.createElement('div') as HTMLDivElement;
        let buttonAdd = document.createElement('button') as HTMLButtonElement;
        buttonAdd.className = 'add';
        buttonAdd.id = 'btn-save';
        buttonAdd.innerText = 'Add';

        blockBtn.appendChild(buttonAdd);
        formCart.appendChild(blockBtn);
        form.appendChild(formCart);
    }
}
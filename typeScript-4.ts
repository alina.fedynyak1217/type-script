//1
function checkPhone(phone: RegExp): boolean {
    return /^((\+38)?\(?\d{3}\)?[\s\.-]?(\d{7}|\d{3}[\s\.-]\d{2}[\s\.-]\d{2}|\d{3}-\d{4}))$/g.test(String(phone));
}
//2
function checkEmail(email: RegExp): boolean {
    return /^[a-zA-Z0-9\.]+\@[a-zA-Z0-9]+\.[a-zA-Z0-9]+$/.test(String(email));
}
//3
function checkSite(site: RegExp): boolean {
    return /^(http\:\/\/|https\:\/\/)?([a-z0-9][a-z0-9\-]*\.)+[a-z0-9][a-z0-9\-]*$/.test(String(site));
}
//4
function checkPassword(password: RegExp): boolean {
    return /^[a-zA-Z0-9_]{6,25}$/.test(String(password));
}
//5
function checkIpvAddress(address: RegExp): boolean {
    return /^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(String(address));
}
//1
interface Array<T> {
    bubbleSort(Fn: ((a: T, b: T) => boolean) ): Array<T>;
    selectionSort(Fn: ((a: T, b: T) => boolean) ): Array<T>;
}
const array: Array<number> = [5, 4, 1, 3, 2];
Array.prototype.bubbleSort = function<T>(callback: ((a: T, b: T) => boolean)): Array<T> {
    for (let j: number = this.length - 1; j > 0; j--) {
        for (let i: number = 0; i < j; i++) {
            if (callback(this[i], this[i + 1])) {
                let temp = this[i];
                this[i] = this[i + 1];
                this[i + 1] = temp;
            }
        }
    }
    return this;
};
array.bubbleSort((a, b) => a > b);
//2
const arrayNumber: Array<number> = [5, 4, 1, 3, 2];
Array.prototype.selectionSort = function<T>(callback: ((a: T, b: T) => boolean)): Array<T> {
    for (let i: number = 0; i < this.length - 1; i++) {
        let min: number = i;

        for (let j: number = i + 1; j < this.length; j++) {
            if (callback(this[j],this[min])) {
                min = j;
            }
        }
        let result = this[min];
        this[min] = this[i];
        this[i] = result;
    }
    return this;
};
arrayNumber.selectionSort((a, b) => a < b);
//3
class BinaryTree {
    root: null | BinaryTree;
    value: number;
    left: null | BinaryTree;
    right: null | BinaryTree;
    constructor(value: number) {
        this.root = null;
        this.value = value;
        this.left = null;
        this.right = null;
    }

    insert(value: number) {
        let newItem = new BinaryTree(value);

        if (!this.root) {
            this.root = newItem;
            return;
        }
        let root = this.root;

        while (root) {
            if (newItem.value < root.value) {
                if (!root.left) {
                    root.left = newItem;
                    return;
                }
                root = root.left;
            } else {
                if (!root.right) {
                    root.right = newItem;
                    return;
                }
                root = root.right;
            }
        }
    }

    search(item: null | BinaryTree, value: number): null | BinaryTree {
        if ( item === null ) {
            return null;
        } else if ( value < item.value ) {
            return this.search(item.left, value);
        } else if  ( value > item.value ) {
            return this.search(item.right, value)
        }

        return item;
    }

    minElement(item: null | BinaryTree): null | BinaryTree {
        if(item === null){
            return null;
        }

        if (item.left === null) {
            return item;
        }
        return item;
    }

    remove(item: null | BinaryTree, value: number): null | BinaryTree {
        if (item === null) {
            return null;
        } else if (value < item.value) {
            item.left = this.remove(item.left, value);
            return item;
        } else if (value > item.value) {
            item.right = this.remove(item.right, value);
            return item;
        } else {
            if (item.left === null && item.right === null) {
                item = null;
                return item;
            }

            if (item.left === null) {
                item = item.right;
                return item;

            } else if (item.right === null) {
                item = item.left;
                return item
            }
        }

        let newItem: null | BinaryTree | any = this.minElement(item.right);
        item.value = newItem.value;
        item.right = this.remove(item.right, newItem.value);
        return newItem;
    }
}

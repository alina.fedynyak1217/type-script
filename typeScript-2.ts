//1
interface Function {
    myBind(obj: object): Function;
}
Function.prototype.myBind = function (context: {}, ...args: any) {
    return (...rest: any) => {
        let bufferObj = Object.create(context);
        bufferObj.func = this;
        let result = bufferObj.func(...args, ...rest);
        return result;
    }
};
//2
interface Function {
    myCall(obj: object): Function;
}
Function.prototype.myCall = function(thisArg: {} | any,...args: any[]){
    if(typeof this != 'function'){
        throw new Error('the caller must be a function');
    }
    if(thisArg === null || thisArg === undefined){
        thisArg = window;
    } else {
        thisArg = new Object(thisArg);
    }
    thisArg.fn = this;

    let result = thisArg.fn(...args);
    delete thisArg.fn;
    return result;
};
//3
interface Array<T> {
    myMap<T>(fn: (value: T, index?: number, arr?: Array<T>) => T) : Array<T>;
}

Array.prototype.myMap = function<T>(callback: (value: T, index?: number, arr?: Array<T>) => T) : Array<T> {
    let result: Array<T> = [];

    for (let i: number = 0; i < this.length; i++) {
        result.push(callback(this[i], i, this));
    }

    return result;
};
const arr: Array<number> = [1,2,3];
let newArr: Array<number> = arr.myMap<number>(function(item: number): number {
    return item * 2;
});
//4
interface Array<T> {
    myFilter<T>(fn: (value: T, index?: number, arr?: Array<T>) => T) : Array<T>;
}

Array.prototype.myFilter = function<T>(callback: (value: T, index?: number, arr?: Array<T>) => T) : Array<T> {
    let result: Array<T> = [];

    for(let i: number = 0; i < this.length; i++) {
        if(callback(this[i], i, this)) {
            result.push(this[i]);
        }
    }

    return result;
};
const Arr: Array<number> = [1,2,3];
let arrFilter: Array<number> = Arr.myFilter(function(item: number): number | any {
    if (item % 2 === 0) {
        return true;
    }
});
//5
interface Array<T> {
    myForEach<T>(fn: (value: T, index?: number, arr?: Array<T>) => T) : Array<T>;
}
Array.prototype.myForEach = function<T>(callback: (value: T, index?: number, arr?: Array<T>) => T): Array<T> {
    const result = [];

    for (let i: number = 0; i < this.length; i++) {
        result.push(callback(this[i]));
    }
    return result;
};

let arrayNumb: Array<number> = [1, 4, 3, 8, 10];
let newArrayNumb: Array<number> = [];

arrayNumb.myForEach<number>(function(item: number): number | any {
    if (item % 2 === 0) {
        newArrayNumb.push(item);
    }
});
//6
function* genFibonacci(num: number) {
    let prev: number = 0;
    let next: number = 1;

    for(let i: number = 0; i < num; i++){
        let temp: number = next;
        next = prev + next;
        yield prev;
        prev = temp;
    }
}
let result = genFibonacci(5);
result.next();
result.next();
result.next();
result.next();
result.next();
//7
const fibonacciIterator = {
    [Symbol.iterator]() {
        let prev: number = 0;
        let next: number = 1;

        return {
            next() {
                let value: number = next;
                next = prev + next;
                prev = value;
                return {value, done: false};
            }
        }
    }
};
for (const num of fibonacciIterator) {
    if (num > 8) break;
}
const resulFib = fibonacciIterator[Symbol.iterator]();
resulFib.next();
resulFib.next();
resulFib.next();
resulFib.next();
resulFib.next();



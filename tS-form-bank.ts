export class Form {
    createForm() {
        let form = document.getElementById('form-bank') as HTMLFormElement;
        if (form.length !== undefined) {
            return;
        }

        let formCart = document.createElement('form') as HTMLFormElement;
        formCart.className = 'form';
        formCart.id = 'form';
        let formTitle = document.createElement('h1') as HTMLHeadingElement;
        formTitle.innerText = 'Form Add';
        let blockName = document.createElement('div') as HTMLDivElement;
        let nameLabel = document.createElement('label') as HTMLLabelElement;
        nameLabel.htmlFor = 'name';
        nameLabel.innerText = 'Name:';
        let nameInput = document.createElement('input') as HTMLInputElement;
        nameInput.type = 'text';
        nameInput.name = 'name';
        nameInput.id = 'name';
        nameInput.value = '';

        blockName.appendChild(nameLabel);
        blockName.appendChild(nameInput);

        formCart.appendChild(formTitle);
        formCart.appendChild(blockName);

        let blockSurname = document.createElement('div') as HTMLDivElement;
        let surnameLabel = document.createElement('label') as HTMLLabelElement;
        surnameLabel.htmlFor = 'surname';
        surnameLabel.innerText = 'Surname:';
        let surnameInput = document.createElement('input') as HTMLInputElement;
        surnameInput.type = 'text';
        surnameInput.name = 'surname';
        surnameInput.id = 'surname';
        surnameInput.value = '';

        blockSurname.appendChild(surnameLabel);
        blockSurname.appendChild(surnameInput);
        formCart.appendChild(blockSurname);

        let blockIsActive = document.createElement('div') as HTMLDivElement;
        blockIsActive.className = 'checkbox';
        let blockIsActiveInput = document.createElement('input') as HTMLInputElement;
        blockIsActiveInput.type = 'checkbox';
        blockIsActiveInput.name = 'isActive';
        blockIsActiveInput.id = 'isActive';
        let blockIsActiveLabel = document.createElement('label') as HTMLLabelElement;
        blockIsActiveLabel.htmlFor = 'isActive';
        blockIsActiveLabel.innerText = 'isActive';

        blockIsActive.appendChild(blockIsActiveInput);
        blockIsActive.appendChild(blockIsActiveLabel);
        formCart.appendChild(blockIsActive);

        let blockRegistration = document.createElement('div') as HTMLDivElement;
        let registrationLabel = document.createElement('label') as HTMLLabelElement;
        registrationLabel.htmlFor = 'registration';
        registrationLabel.innerText = 'Registration:';
        let registrationInput = document.createElement('input') as HTMLInputElement;
        registrationInput.type = 'date';
        registrationInput.name = 'registration';
        registrationInput.id = 'registration';
        registrationInput.value = '';

        blockRegistration.appendChild(registrationLabel);
        blockRegistration.appendChild(registrationInput);
        formCart.appendChild(blockRegistration);

        let blockCarts = document.createElement('div') as HTMLDivElement;
        let blockCartsTitle = document.createElement('h2') as HTMLHeadingElement;
        blockCartsTitle.innerText = 'Carts:';
        blockCartsTitle.id = 'title-cart';

        blockCarts.appendChild(blockCartsTitle);
        formCart.appendChild(blockCarts);

        let blockDebit = document.createElement('div') as HTMLDivElement;
        blockDebit.className = 'debit-block';
        let debitCheckbox = document.createElement('div') as HTMLDivElement;
        debitCheckbox.className = 'checkbox';
        let debitInput = document.createElement('input') as HTMLInputElement;
        debitInput.type = 'checkbox';
        debitInput.name = 'debit';
        debitInput.id = 'debit';
        debitInput.value = 'debit';
        let debitLabel = document.createElement('label') as HTMLLabelElement;
        debitLabel.htmlFor = 'debit';
        debitLabel.innerText = 'Debit:';

        debitCheckbox.appendChild(debitInput);
        debitCheckbox.appendChild(debitLabel);
        blockDebit.appendChild(debitCheckbox);

        let blockDebitCountMoney = document.createElement('div') as HTMLDivElement;
        let debitCountMoneyInput = document.createElement('input') as HTMLInputElement;
        debitCountMoneyInput.type = 'text';
        debitCountMoneyInput.name = 'debitCountMoney';
        debitCountMoneyInput.id = 'debitCountMoney';
        debitCountMoneyInput.value = '';
        let debitCountMoneyLabel = document.createElement('label') as HTMLLabelElement;
        debitCountMoneyLabel.htmlFor = 'debitCountMoney';
        debitCountMoneyLabel.innerText = 'Count Money:';

        blockDebitCountMoney.appendChild(debitCountMoneyLabel);
        blockDebitCountMoney.appendChild(debitCountMoneyInput);
        blockDebit.appendChild(blockDebitCountMoney);

        let blockDebitExpairetAt = document.createElement('div') as HTMLDivElement;
        let debitExpairetAtInput = document.createElement('input') as HTMLInputElement;
        debitExpairetAtInput.type = 'date';
        debitExpairetAtInput.name = 'debitExpairetAt';
        debitExpairetAtInput.id = 'debitExpairetAt';
        debitExpairetAtInput.value = '';
        let debitExpairetAtLabel = document.createElement('label') as HTMLLabelElement;
        debitExpairetAtLabel.htmlFor = 'debitExpairetAt';
        debitExpairetAtLabel.innerText = 'Expairet At:';

        blockDebitExpairetAt.appendChild(debitExpairetAtLabel);
        blockDebitExpairetAt.appendChild(debitExpairetAtInput);
        blockDebit.appendChild(blockDebitExpairetAt);

        let blockDebitLastOperation = document.createElement('div') as HTMLDivElement;
        let debitLastOperationInput = document.createElement('input') as HTMLInputElement;
        debitLastOperationInput.type = 'date';
        debitLastOperationInput.name = 'debitLastOperation';
        debitLastOperationInput.id = 'debitLastOperation';
        debitLastOperationInput.value = '';
        let debitLastOperationLabel = document.createElement('label') as HTMLLabelElement;
        debitLastOperationLabel.htmlFor = 'debitLastOperation';
        debitLastOperationLabel.innerText = 'Last Operation:';

        blockDebitLastOperation.appendChild(debitLastOperationLabel);
        blockDebitLastOperation.appendChild(debitLastOperationInput);
        blockDebit.appendChild(blockDebitLastOperation);

        let blockDebitCurrency = document.createElement('div') as HTMLDivElement;
        let debitCurrencyLabel = document.createElement('label') as HTMLLabelElement;
        debitCurrencyLabel.htmlFor = 'debitCurrency';
        debitCurrencyLabel.innerText = 'currency:';
        let debitCurrencySelect = document.createElement('select') as HTMLSelectElement;
        debitCurrencySelect.id = 'debitCurrency';
        debitCurrencySelect.name = 'debitCurrency';
        let debitOptionUSD = document.createElement('option') as HTMLOptionElement;
        debitOptionUSD.value = 'USD';
        debitOptionUSD.innerText = 'USD';
        let debitOptionRUB = document.createElement('option') as HTMLOptionElement;
        debitOptionRUB.value = 'RUB';
        debitOptionRUB.innerText = 'RUB';
        let debitOptionUAH = document.createElement('option') as HTMLOptionElement;
        debitOptionUAH.value = 'UAH';
        debitOptionUAH.innerText = 'UAH';
        let debitOptionEUR = document.createElement('option') as HTMLOptionElement;
        debitOptionEUR.value = 'EUR';
        debitOptionEUR.innerText = 'EUR';

        blockDebitCurrency.appendChild(debitCurrencyLabel);
        debitCurrencySelect.appendChild(debitOptionUSD);
        debitCurrencySelect.appendChild(debitOptionRUB);
        debitCurrencySelect.appendChild(debitOptionUAH);
        debitCurrencySelect.appendChild(debitOptionEUR);
        blockDebitCurrency.appendChild(debitCurrencySelect);
        blockDebit.appendChild(blockDebitCurrency);
        formCart.appendChild(blockDebit);

        let blockCredit = document.createElement('div') as HTMLDivElement;
        blockCredit.className = 'credit-block';
        let creditCheckbox = document.createElement('div') as HTMLDivElement;
        creditCheckbox.className = 'checkbox';
        let creditInput = document.createElement('input') as HTMLInputElement;
        creditInput.type = 'checkbox';
        creditInput.name = 'credit';
        creditInput.id = 'credit';
        creditInput.value = 'credit';
        let creditLabel = document.createElement('label') as HTMLLabelElement;
        creditLabel.htmlFor = 'credit';
        creditLabel.innerText = 'Credit:';

        creditCheckbox.appendChild(creditInput);
        creditCheckbox.appendChild(creditLabel);
        blockCredit.appendChild(creditCheckbox);

        let blockCountMoney = document.createElement('div') as HTMLDivElement;
        let countMoneyInput = document.createElement('input') as HTMLInputElement;
        countMoneyInput.type = 'text';
        countMoneyInput.name = 'countMoney';
        countMoneyInput.id = 'countMoney';
        countMoneyInput.value = '';
        let countMoneyLabel = document.createElement('label') as HTMLLabelElement;
        countMoneyLabel.htmlFor = 'countMoney';
        countMoneyLabel.innerText = 'Count Money:';

        blockCountMoney.appendChild(countMoneyLabel);
        blockCountMoney.appendChild(countMoneyInput);
        blockCredit.appendChild(blockCountMoney);

        let blockCreditExpairetAt = document.createElement('div') as HTMLDivElement;
        let creditExpairetAtInput = document.createElement('input') as HTMLInputElement;
        creditExpairetAtInput.type = 'date';
        creditExpairetAtInput.name = 'creditExpairetAt';
        creditExpairetAtInput.id = 'creditExpairetAt';
        creditExpairetAtInput.value = '';
        let creditExpairetAtLabel = document.createElement('label') as HTMLLabelElement;
        creditExpairetAtLabel.htmlFor = 'creditExpairetAt';
        creditExpairetAtLabel.innerText = 'Expairet At:';

        blockCreditExpairetAt.appendChild(creditExpairetAtLabel);
        blockCreditExpairetAt.appendChild(creditExpairetAtInput);
        blockCredit.appendChild(blockCreditExpairetAt);

        let blockCreditLastOperation = document.createElement('div') as HTMLDivElement;
        let creditLastOperationInput = document.createElement('input') as HTMLInputElement;
        creditLastOperationInput.type = 'date';
        creditLastOperationInput.name = 'creditLastOperation';
        creditLastOperationInput.id = 'creditLastOperation';
        creditLastOperationInput.value = '';
        let creditLastOperationLabel = document.createElement('label') as HTMLLabelElement;
        creditLastOperationLabel.htmlFor = 'creditLastOperation';
        creditLastOperationLabel.innerText = 'Last Operation:';

        blockCreditLastOperation.appendChild(creditLastOperationLabel);
        blockCreditLastOperation.appendChild(creditLastOperationInput);
        blockDebit.appendChild(blockCreditLastOperation);

        let blockCreditLimit = document.createElement('div') as HTMLDivElement;
        let creditLimitInput = document.createElement('input') as HTMLInputElement;
        creditLimitInput.type = 'text';
        creditLimitInput.name = 'creditLimit';
        creditLimitInput.id = 'creditLimit';
        creditLimitInput.value = '';
        let creditLimitLabel = document.createElement('label') as HTMLLabelElement;
        creditLimitLabel.htmlFor = 'creditLimit';
        creditLimitLabel.innerText = 'Сredit Limit:';

        blockCreditLimit.appendChild(creditLimitLabel);
        blockCreditLimit.appendChild(creditLimitInput);
        blockCredit.appendChild(blockCreditLimit);

        let blockCreditCountMoney = document.createElement('div') as HTMLDivElement;
        let creditCountMoneyInput = document.createElement('input') as HTMLInputElement;
        creditCountMoneyInput.type = 'text';
        creditCountMoneyInput.name = 'creditCountMoney';
        creditCountMoneyInput.id = 'creditCountMoney';
        creditCountMoneyInput.value = '';
        let creditCountMoneyLabel = document.createElement('label') as HTMLLabelElement;
        creditCountMoneyLabel.htmlFor = 'creditCountMoney';
        creditCountMoneyLabel.innerText = 'Сredit Count Money:';

        blockCreditCountMoney.appendChild(creditCountMoneyLabel);
        blockCreditCountMoney.appendChild(creditCountMoneyInput);
        blockCredit.appendChild(blockCreditCountMoney);

        let blockCreditCurrency = document.createElement('div') as HTMLDivElement;
        let creditCurrencyLabel = document.createElement('label') as HTMLLabelElement;
        creditCurrencyLabel.htmlFor = 'creditCurrency';
        creditCurrencyLabel.innerText = 'currency:';
        let creditCurrencySelect = document.createElement('select') as HTMLSelectElement;
        creditCurrencySelect.id = 'creditCurrency';
        creditCurrencySelect.name = 'creditCurrency';
        let creditOptionUSD = document.createElement('option') as HTMLOptionElement;
        creditOptionUSD.value = 'USD';
        creditOptionUSD.innerText = 'USD';
        let creditOptionRUB = document.createElement('option') as HTMLOptionElement;
        creditOptionRUB.value = 'RUB';
        creditOptionRUB.innerText = 'RUB';
        let creditOptionUAH = document.createElement('option') as HTMLOptionElement;
        creditOptionUAH.value = 'UAH';
        creditOptionUAH.innerText = 'UAH';
        let creditOptionEUR = document.createElement('option') as HTMLOptionElement;
        creditOptionEUR.value = 'EUR';
        creditOptionEUR.innerText = 'EUR';

        blockCreditCurrency.appendChild(creditCurrencyLabel);
        creditCurrencySelect.appendChild(creditOptionUSD);
        creditCurrencySelect.appendChild(creditOptionRUB);
        creditCurrencySelect.appendChild(creditOptionUAH);
        creditCurrencySelect.appendChild(creditOptionEUR);
        blockCreditCurrency.appendChild(creditCurrencySelect);
        blockCredit.appendChild(blockCreditCurrency);
        blockDebit.appendChild(blockCredit);
        formCart.appendChild(blockCredit);


        let blockBtn = document.createElement('div') as HTMLDivElement;
        blockBtn.className = 'btn-add';
        blockBtn.id = 'btn-save';
        let buttonAdd = document.createElement('button') as HTMLButtonElement;
        buttonAdd.className = 'add';
        buttonAdd.innerText = 'Add';

        blockBtn.appendChild(buttonAdd);
        formCart.appendChild(blockBtn);
        form.appendChild(formCart);
    }
}